import { DbsppsManagementConsolePage } from './app.po';

describe('dbspps-management-console App', () => {
  let page: DbsppsManagementConsolePage;

  beforeEach(() => {
    page = new DbsppsManagementConsolePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
