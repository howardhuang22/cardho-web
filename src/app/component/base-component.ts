import { LpdmService } from './../api/lpdm.service';
import { OnInit } from '@angular/core';
import { Search } from 'app/model/search';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Page } from "app/model/page";
import { ApiService } from "app/service/api.service";
import { CurrentItem } from "app/model/current-item";
import { ShareService } from "app/service/share.service";

export class BaseComponent<T> {

    list: any[];
    typeName: string | undefined;
    currentItem: CurrentItem<T>; // 新增/編輯 dialog 中的 item
    modalRef: NgbModalRef; // Dialog 物件參考，用來關閉 dialog

    constructor(protected api: ApiService, protected alert: AlertService, protected base: BaseService,
        protected modalService: NgbModal, public share: ShareService, type?: { new (): T; }, protected lpdmService?: LpdmService) {
        this.typeName = type && type.name;
    }

    doSearch(callback?: Function) {
        this.alert.clear();
        this.share.loading = true;
        this.share.search.page = this.share.page.page - 1; // 頁面起始第1頁，java起始第0頁
        this.refreshData(null, callback);
        // this.refreshData('查詢');
    }

    addFormData (item) {
        // this.share.search = new Search();
        this.api.saveFormData(item).subscribe((res) => this.refreshData('新增'));
    }
    
    editFormData (item) {
        // this.share.search = new Search();
        this.api.updateFormData(item.bodyJson.id, item).subscribe((res) => this.refreshData('編輯'));
    }

    add(item) {
        // this.share.search = new Search();
        this.api.save(item).subscribe((res) => this.refreshData('新增'));
    }

    edit(item) {
        // this.share.search = new Search();
        // this.api.update(item).subscribe((res) => this.refreshData('編輯'));
        this.api.updateById(item.id, item).subscribe((res) => this.refreshData('編輯'));
    }

    remove(id, name, item?: any) {
        // this.share.search = new Search();
        if (confirm('確認刪除 ' + name + ' ?')) {
            item && (item.removing = true);
            this.api.remove(id).subscribe((res) => this.refreshData('刪除'));
        }
    }
    //
    // check(id, item?: any) {
    //     item.checking = true;
    //     this.api.check(id).subscribe((res) => this.refreshData('覆核'));
    // }
    //
    // recover(id, item?: any) {
    //     item.recovering = true;
    //     this.api.recover(id).subscribe((res) => this.refreshData('回復'));
    // }
    //
    // unlock(id, item?: any) {
    //     item.unlocking = true;
    //     this.api.unlock(id).subscribe((res) => this.refreshData('解鎖'));
    // }
    //
    // execute(id, item?: any) {
    //     if (confirm('確定執行?')) {
    //         item.executeing = true;
    //         this.api.execute(id).subscribe((res) => this.refreshData('執行'));
    //     }
    // }

    // print(fileName) {
    //     this.share.printing = true;
    //     this.api.print(this.share.search, fileName);
    // }


    /**
     * 新增/修改 跳出視窗
     * 新增時資料為空，只傳content
     * 編輯時傳content + data + canSave
     * MerchantGroup, Merchant 等頁面開視窗時需寫log到lpdmdata
     *
     * @param content 視窗物件
     * @param data 要編輯的資料
     * @param canSave 跳出視窗是否顯示儲存按鈕
     */
    setCurrentItem(content: NgbModal, data, canSave = true) {
        this.currentItem = {
            canSave: canSave,
            item: data
        }
        data && this.logToLpdmData(data);
        this.openDialog(content);
        this.share.dialog = this.modalRef;
    }

    executeBatch(id) {
        if (confirm("是否確認執行")) {
            this.share.executing = true;
            this.api.execute(id).subscribe((res) => {
                res && res.content && this.alert.error(res.content);
                this.share.executing = false;
            });
        }
    }

    listFiles(id) {
        this.share.loading = true;
        this.api.listFiles(id).subscribe((res) => {
            this.list = res || [];
            if (!this.list.length) {
                this.alert.error('目錄中無檔案');
            }
            this.share.loading = false;
        });
    }

    get currentUserId() {
        return this.base.user.userId;
    }

    get auths() {
        return this.base.auths;
    }

    // 重新查詢並更新頁面資料
    private refreshData(successText: string, callback?: Function) {
        console.log('refreshData');
        this.api.query(this.share.search).subscribe((res) => {
            // console.log(res);
            this.list = Object.assign([], res.list);
            console.log(this.list);
            this.closeDialog();
            this.share.loading = false;
            !this.list.length && this.alert.error('查無資料');
            callback && callback(this);
            res.page && this.updatePagination(res.page);
            successText && this.alert.success(successText + '完成');
        });
    }
    
    private updatePagination(page) {
        this.share.page = {
            total: page.totalElements,
            page: page.number + 1,
            size: page.size
        };
    }

    private closeDialog() {
        this.modalRef && this.modalRef.close();
    }

    private logToLpdmData(data) {
        switch (this.typeName) {
            case 'MerchantGroup':
                this.lpdmService.log(data.merchantGroupId, 'MERCHANT_GROUP').subscribe();
                break;
            case 'Merchant':
                this.lpdmService.log(data.merchantId, 'MERCHANT').subscribe();
                break;
            default:
                break;
        }
    }

    private openDialog(content: NgbModal) {
        let dialogClass = 'dialog ';
        this.typeName === 'TimePolicy' && (dialogClass += 'lg');
        this.typeName === 'PoDetailNccc' && (dialogClass += 'lg nccc');
        this.modalRef = this.modalService.open(content, { windowClass: dialogClass });
    }

}
