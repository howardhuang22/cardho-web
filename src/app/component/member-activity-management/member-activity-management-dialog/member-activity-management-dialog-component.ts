/**
 * Created by Howard on 2017/7/11.
 */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BaseDialog} from "../../base-dialog";
// import {MerchantGroup} from "../../../model/merchant-group";
import {ShareService} from "../../../service/share.service";
import {MemberUser} from "../../../model/member-user";
import {MemberActivity} from "../../../model/member-activity";
import {MemberPageSetupService} from "../../../api/memberPageSetup.service";
import {MemberPageSetup} from "../../../model/member-pageSetup";
import {MemberTreasure} from "../../../model/member-treasure";
import {MemberTreasureService} from "../../../api/memberTreasure.service";

@Component({
    selector: 'member-activity-management-dialog',
    templateUrl: './member-activity-management-dialog-component.html',
    styleUrls: ['./member-activity-management-dialog-component.css']
})
export class MemberActivityManagementDialogComponent extends BaseDialog<MemberActivity> {
    
    pageSetupList: MemberPageSetup[] = [];
    treasureList: MemberTreasure[] = [];
    constructor(public share: ShareService, private memeberPageSetupService: MemberPageSetupService, private memberTreasureService: MemberTreasureService) {
        super(share);
    }

    ngOnInit() {
        this.memeberPageSetupService.getList().subscribe(res => this.pageSetupList = res.list);
        this.memberTreasureService.getList().subscribe(res => this.treasureList = res.list);
        if (this.item == null) {
            this.item = new MemberActivity();
            this.item.isActived = 0;
        }
    }

    edit(editedItem) {
        this.share.loading = true;
        for (let key in this.item) {
            if (key == 'isActived') {
                this.item[key] = editedItem[key];
            } else if (editedItem[key]) {
                this.item[key] = editedItem[key];
            }
        }
        if (editedItem.objectId) {
            this.item.objectId = editedItem['objectId'];
        }

        if (editedItem.pageId) {
            this.item.pageId = editedItem['pageId'];
        }
        this.onEdit.emit(this.item);
    }
}
