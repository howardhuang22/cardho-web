import {BaseComponent} from "../base-component";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component } from '@angular/core';
import {AlertService} from "../../service/alert.service";
import {BaseService} from "../../service/base.service";
import {ShareService} from "../../service/share.service";
import {MemberActivity} from "../../model/member-activity";
import {MemberActivityService} from "../../api/memberActivity.service";
/**
 * Created by Howard on 2017/7/5.
 */


@Component({
    selector: 'member-activity-management',
    templateUrl: './member-activity-management-component.html',
    styleUrls: ['./member-activity-management-component.css']
})
export class MemberActivityManagementComponent extends BaseComponent<MemberActivity>{

    searchColumns = 'activityId, activityDescription, activityObjectId, pageId, index, isActived , add, doSearch';
    //
    constructor(protected memberActivityService: MemberActivityService, protected alert: AlertService, protected base: BaseService,
                protected modalService: NgbModal, public share: ShareService) {
        super(memberActivityService, alert, base, modalService, share, MemberActivity);
    }

}