import { ShareService } from 'app/service/share.service';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Output, EventEmitter, Input } from '@angular/core';
import { CurrentItem } from "app/model/current-item";
export class BaseDialog<T> {

  constructor(public share: ShareService, protected type?: { new (): T; }) { }
  
  @Output() onAdd = new EventEmitter<any>();
  @Output() onEdit = new EventEmitter<any>();
  @Output() onExecute = new EventEmitter<any>();
  @Output() onAddFormData = new EventEmitter<any>();
  @Output() onEditFormData = new EventEmitter<any>();

  item: T;
  canSave: boolean;

  @Input() modalRef: NgbModalRef;

  @Input()
  set currentItem(currentItem: CurrentItem<T>) {
    this.item = currentItem.item || this.type && new this.type();
    this.canSave = currentItem.canSave;
  }

  add(item) {
    this.share.loading = true;
    this.onAdd.emit(item);
  }

  edit(editedItem) {
    this.share.loading = true;
    for (let key in this.item) {
      if (key == 'enabled') {
        this.item[key] = editedItem[key];
      } else if (editedItem[key]) {
        this.item[key] = editedItem[key];
      }
    }
    this.onEdit.emit(this.item);
  }

  execute(id) {
    this.share.loading = true;
    this.onExecute.emit(id);
  }

  close() {
    this.modalRef.close();
  }

}
