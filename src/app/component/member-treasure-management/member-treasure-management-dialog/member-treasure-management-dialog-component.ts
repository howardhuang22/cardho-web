/**
 * Created by Howard on 2017/7/11.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BaseDialog} from "../../base-dialog";
import {ShareService} from "../../../service/share.service";
import {MemberTreasure} from "../../../model/member-treasure";
import {MemberPageSetupService} from "../../../api/memberPageSetup.service";
import {MemberPageSetup} from "../../../model/member-pageSetup";
import {MemberTreasureService} from "../../../api/memberTreasure.service";

@Component({
    selector: 'app-member-treasure-management-dialog',
    templateUrl: './member-treasure-management-dialog-component.html',
    styleUrls: ['./member-treasure-management-dialog-component.css']
})

export class MemberTresureManagementDialogComponent extends BaseDialog<MemberTreasure> {
    constructor(public share: ShareService, private memberTreasureService: MemberTreasureService, private memeberPageSetupService: MemberPageSetupService) {
        super(share);
    }
    
    pageSetupList: MemberPageSetup[] = [];
    file : File;

    ngOnInit() {
        this.memeberPageSetupService.getList().subscribe(res => this.pageSetupList = res.list);
        if (this.item == null) {
            this.item = new MemberTreasure();
            this.item.index = 0;
            this.item.enabled = 0;
        }
    }
    
    fileSelected (event) {
        let fileList : FileList = event.target.files;
        this.file = fileList[0];
    }
    
    downloadFile () {
        // this.memberTreasureService.download(this.item)
        //     .subscribe(data => this.download(data)),//console.log(data),
        //     error => console.log("Error downloading the file."),
        //     () => console.log('Completed file download.');
        this.memberTreasureService.download(this.item);
    }

    download(data){
        var blob = new Blob([(<any>data)], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
        // var blob = new Blob([(<any>data)], {});
        var url= window.URL.createObjectURL(blob);
        window.open(url);
        console.log();
    }

    addFormData (item) {
        this.share.loading = true;
        let object = {
            bodyJson : item,
            file : this.file
        }
        this.onAddFormData.emit(object);
    }

    editFormData (item) {
        this.share.loading = true;
        let object = {
            bodyJson : item,
            file : this.file
        }
        this.onEditFormData.emit(object);
    }
}
