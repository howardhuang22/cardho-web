/**
 * Created by Howard on 2017/7/5.
 */
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import {LpdmService} from "../../api/lpdm.service";
import {ShareService} from "../../service/share.service";
import {BaseDialog} from "../base-dialog";
import {Groupv2User} from "../../model/groupv2-user";
import {Groupv2UserService} from "../../api/groupv2-user.service";
import {MemberTreasure} from "../../model/member-treasure";
import {MemberTreasureService} from "../../api/memberTreasure.service";



@Component({
    selector: 'app-member-treasure-management',
    templateUrl: './member-treasure-management-component.html',
    styleUrls: ['./member-treasure-management-component.css']
})

export class MemberTreasureManagementComponent  extends BaseComponent<MemberTreasure>{

    searchColumns = 'treasureId, treasureName, treasureType, treasureCode, pageId, index, enabled , add, doSearch';
    // searchColumns = 'merchantGroupIdLike, merchantGroupName, enabled, checked, searchIndex, add, print, insertDate, modifyDate, insertUser, doSearch';
    // searchIndexContent = '總店代碼 + 建檔日期';
    //
    constructor(protected memberTreasureService: MemberTreasureService, protected alert: AlertService, protected base: BaseService,
                protected modalService: NgbModal, public share: ShareService) {
        super(memberTreasureService, alert, base, modalService, share, MemberTreasure);
    }
}