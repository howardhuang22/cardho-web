/**
 * Created by skylee on 2017/7/10.
 */

import { Groupv2 } from './../../../model/groupv2';
import { ShareService } from 'app/service/share.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentItem } from './../../../model/current-item';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseDialog } from "app/component/base-dialog";
import { MemberUser } from "../../../model/member-user";
import {MemberUserService} from "../../../api/memberUser.service";

@Component({
    selector: 'app-member-user-management-dialog',
    templateUrl: './member-user-management-dialog-component.html',
    styleUrls: ['./member-user-management-dialog-component.css']
})
export class MemberUserManagementDialogComponent extends BaseDialog<MemberUser> {
    constructor(public share: ShareService, private memberUserService:MemberUserService) {
        super(share);
        
    }
    
    ngOnInit() {
        if (this.item == null) {
            this.item = new MemberUser();
            this.item.enabled = 0;
            this.item.signType = "0";
        }
    }
}
