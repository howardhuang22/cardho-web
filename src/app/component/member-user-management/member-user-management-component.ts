/**
 * Created by skylee on 2017/7/5.
 */

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import { ShareService } from "app/service/share.service";


import { MemberUserService } from  "app/api/memberUser.service"
import { MemberUser } from './../../model/member-user';

@Component({
    selector: 'app-member-user-management',
    templateUrl: './member-user-management-component.html',
    styleUrls: ['./member-user-management-component.css']
})
export class MemberUserManagementComponent extends BaseComponent<MemberUser> {

    searchColumns = 'memberUserId, memberUserAccount, memberUserName, memberUserType, memberUserSignType, enabled, add, doSearch';

    constructor(protected memberUserService: MemberUserService, protected alert: AlertService, protected base: BaseService,
                protected modalService: NgbModal, public share: ShareService) {
        super(memberUserService, alert, base, modalService, share, MemberUser);
        // this.api.update = memberUserService.editUser;
    }

    // doSearch () {
    //
    // }
}
