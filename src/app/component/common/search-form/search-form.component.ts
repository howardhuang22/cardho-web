import { DateUtilsService } from 'app/service/date-util.service';
import { User } from 'app/model/user';
import { Groupv2User } from './../../../model/groupv2-user';
import { Groupv2UserService } from 'app/api/groupv2-user.service';
import { ShareService } from './../../../service/share.service';
import { Component, OnInit, Output, Input, SimpleChanges, EventEmitter } from '@angular/core';
import { BaseService } from "app/service/base.service";
import { Search } from "app/model/search";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { NgForm } from "@angular/forms";


@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent {

  constructor(private base: BaseService, public share: ShareService, private groupv2UserService: Groupv2UserService, private dateUtils: DateUtilsService) {
    this.auths = base.auths;
  }

  auths;
  _columns: string;
  groupv2UserList: Groupv2User[] = [];
  search: Search = new Search();
  orderStatusOptions = [
    { value: 'TX', name: '交易', checked: true },
    { value: 'RF', name: '退貨', checked: true },
    { value: 'PR', name: '提前請款', checked: true },
  ];

  @Input() canAdd: boolean;
  @Input() searchIndexContent: string;

  @Output() onSearch = new EventEmitter<any>();
  @Output() onAdd = new EventEmitter<any>();
  @Output() onPrint = new EventEmitter<any>();

  @Input()
  set columns(columns) {
    this._columns = columns;
  }

  get columns() {
    return this._columns;
  }

  ngOnInit() {
    // this.search.checked = this.search.enabled = this.search.insertUser = '';
    // if (this.columns.includes('modifyDate')) {
    //   this.search.modifyBegin = this.search.modifyEnd = this.dateUtils.todayDate();
    // }
    // if (this.columns.includes('insertUser')) {
    //   this.groupv2UserService.query().subscribe(res => this.groupv2UserList = res.list);
    // }
    // this.search.batchType = "03";
    // this.search.orderStatus = ["TX","RF","PR"];
  }

  /**
   * Datepicker的初始值須在頁面初始化完成以後指定 才可正確地把值傳給form
   */
  // ngAfterViewInit() {
  //   this.search.modifyBegin = this.search.modifyEnd = this.dateUtils.todayDate();
  // }

  doSearch(search: Search) {
    this.share.search = search;
    this.onSearch.emit();
  }

  add(search: Search) {
    this.onAdd.emit();
  }

  print(search: Search) {
    this.share.search = search;
    this.onPrint.emit();
  }

  style = {
    label: 'col-sm-2 col-form-label col-form-label-sm pr-0',
    formgroup: 'form-group mb-1 row',
    formcontrol: 'form-control form-control-sm'
  }

}
