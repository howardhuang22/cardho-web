import { MenuService } from 'app/api/menu.service';
import { ShareService } from './../../../service/share.service';
import { Component, OnInit } from '@angular/core';
import { Menu } from "app/model/menu";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {

  menuList: Array<Menu>;

  constructor(private menuService: MenuService, public share: ShareService) {
  }

  ngOnInit() {
    this.menuService.findByUserInSession().subscribe(res => this.menuList = res.list);
    // this.menuService.findByUserType("1");
  }

}
