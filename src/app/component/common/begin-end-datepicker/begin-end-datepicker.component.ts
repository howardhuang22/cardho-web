import { ShareService } from 'app/service/share.service';
import { NgbDateStruct, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Search } from "app/model/search";
import { DateUtilsService } from "app/service/date-util.service";

@Component({
  selector: 'begin-end-datepicker',
  templateUrl: './begin-end-datepicker.component.html',
  styleUrls: ['./begin-end-datepicker.component.css']
})
export class BeginEndDatepickerComponent {

  @Input() label: string;

  @Output() onBegin = new EventEmitter<Date>();
  @Output() onEnd = new EventEmitter<Date>();
  
  _begin: NgbDateStruct;
  _end: NgbDateStruct;

  constructor(private share: ShareService, private dateUtils: DateUtilsService) { }

  @Input()
  set begin(begin: Date) {
    this._begin = this.dateUtils.dateToNgbDate(begin);
  }

  @Input()
  set end(end: Date) {
    this._end = this.dateUtils.dateToNgbDate(end);
  }

  /**
   * 把其他的月曆(share.dplist)關起來
   * @param dp 
   */
  onDpClick(dp: NgbInputDatepicker) {
    dp.toggle();
    this.share.openeddp && this.share.openeddp !== dp && this.share.openeddp.close();
    this.share.openeddp = dp;
  }

  beginChanged(ngbDate: NgbDateStruct) {
    this.onBegin.emit(this.dateUtils.ngbDateToDate(ngbDate));
  }

  endChanged(ngbDate: NgbDateStruct) {
    this.onEnd.emit(this.dateUtils.ngbDateToDate(ngbDate));
  }

}
