import { Component, OnInit } from '@angular/core';
import { ShareService } from "app/service/share.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(public share: ShareService) {
    share.loading = false;
  }

  ngOnInit() {
  }

}
