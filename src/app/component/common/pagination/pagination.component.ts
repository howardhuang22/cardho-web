import { Page } from 'app/model/page';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ShareService } from "app/service/share.service";

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent {

  @Output()
  onPageChange = new EventEmitter<any>();

  constructor(public share: ShareService) { }

  pageChange() {
    this.onPageChange.emit();
  }

}
