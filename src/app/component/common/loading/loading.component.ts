import { ShareService } from 'app/service/share.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

  @Input() check = true;  // 是否要使用share.loading來控制loading顯示

  constructor(public share: ShareService) { }

  ngOnInit() {
  }

}
