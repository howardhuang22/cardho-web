import { Groupv2 } from './../../../model/groupv2';
import { ShareService } from 'app/service/share.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentItem } from './../../../model/current-item';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseDialog } from "app/component/base-dialog";

@Component({
  selector: 'app-groupv2-management-dialog',
  templateUrl: './groupv2-management-dialog.component.html',
  styleUrls: ['./groupv2-management-dialog.component.css']
})
export class Groupv2ManagementDialogComponent extends BaseDialog<Groupv2> {
  constructor(public share: ShareService) {
    super(share);
  }  
}
