import { Groupv2 } from './../../model/groupv2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import { Groupv2Service } from "app/api/groupv2.service";
import { ShareService } from "app/service/share.service";

@Component({
  selector: 'app-groupv2-management',
  templateUrl: './groupv2-management.component.html',
  styleUrls: ['./groupv2-management.component.css']
})
export class Groupv2ManagementComponent extends BaseComponent<Groupv2> {

  searchColumns = 'groupv2IdLike, groupv2Name, enabled, checked, searchIndex, add, print, insertDate, modifyDate, insertUser, doSearch';
  searchIndexContent = '群組代碼 + 建檔日期';

  constructor(protected groupv2Service: Groupv2Service, protected alert: AlertService, protected base: BaseService,
    protected modalService: NgbModal, public share: ShareService) {
    super(groupv2Service, alert, base, modalService, share, Groupv2);
  }

}
