import { BaseService } from 'app/service/base.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AlertService } from "app/service/alert.service";
import { AuthService } from "app/service/auth.service";
import { ShareService } from "app/service/share.service";

@Component({
    selector: 'app-login',
    templateUrl: './login-management-component.html',
    styleUrls: ['./login-management-component.css']
})
export class LoginComponent implements OnInit {
    model: any = {};
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private alertService: AlertService,
        private baseService: BaseService,
        public share: ShareService) { }

    ngOnInit() {
        // reset login status
        this.authService.logout();
        this.share.loading = false;
        this.model = {
            username : "Admin",
            password : "12345"
        }

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/loginComponent';
        // console.log(this.route.snapshot.queryParams['returnUrl']);
    }

    login() {
        this.share.loading = true;
        this.authService.login(this.model.username, this.model.password).subscribe(
            data => {
                // this.router.navigate(['/main/' + data.loginPageUrl]);
                this.router.navigate(['/main/memberUserManagement']);
            },
            error => {
                console.log('Login error', error);
                this.alertService.error(error && error.json().message);
                this.share.loading = false;
        });
    }
}
