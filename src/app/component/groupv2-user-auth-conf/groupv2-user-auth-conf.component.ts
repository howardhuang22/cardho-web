import { MainmenuService } from 'app/api/mainmenu.service';
import { Mainmenu } from 'app/model/mainmenu';
// import { PoDetailNcccRefundComponent } from './../po-detail-nccc-refund/po-detail-nccc-refund.component';
import { Submenu } from 'app/model/submenu';
import { Subject } from 'rxjs/Subject';
import { Action } from './../../model/action';
import { Menu } from './../../model/menu';
import { UserAuthority } from './../../model/user-authority';
import { Groupv2Service } from 'app/api/groupv2.service';
import { Groupv2 } from './../../model/groupv2';
import { UserAuthorityService } from 'app/api/user-authority.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import { ShareService } from "app/service/share.service";

@Component({
  selector: 'app-groupv2-user-auth-conf',
  templateUrl: './groupv2-user-auth-conf.component.html',
  styleUrls: ['./groupv2-user-auth-conf.component.css']
})
export class Groupv2UserAuthConfComponent extends BaseComponent<Menu> {

  constructor(protected mainmenuService: MainmenuService, protected alert: AlertService, protected base: BaseService,
    protected modalService: NgbModal, public share: ShareService, private userAuthorityService: UserAuthorityService,
    private groupv2Service: Groupv2Service) {
    super(mainmenuService, alert, base, modalService, share);
  }

  groupv2List: Groupv2[];
  groupv2Auths: UserAuthority[];
  groupv2Id;

  /**
   * 1. 查詢所有menu/submenu/action
   * 2. 查詢groupv2 list
   * 3. 查詢第一個groupv2的權限
   * 4. 設定頁面check狀態
   */
  ngOnInit() {
    this.doSearch(this.queryGroupv2List);
  }

  /**
   * 依照groupv2Id查詢auths
   * @param groupv2Id 
   */
  queryAuths(groupv2Id, successText = '', checkSubmenu = null) {
    this.alert.clear();
    successText && this.alert.success(successText);
    this.groupv2Id = groupv2Id;
    this.share.loading = true;
    this.userAuthorityService.queryByGroupv2Id(groupv2Id).subscribe((res) => {
      this.groupv2Auths = res.list;
      this.share.loading = false;
      checkSubmenu && (checkSubmenu.checking = false);
      if (this.list && this.groupv2Auths) {
        this.updateAuthForm();
      } else {
        this.clearAuthForm();
      }
    });
  }

  saveAuths() {
    this.alert.clear();
    this.share.loading = true;
    let auths: UserAuthority[] = [];
    let hasDefaultPage = false;
    this.list.forEach(m => m.submenus.forEach(s => s.actions.forEach(a => {
      if (a.checked) {
        let auth = new UserAuthority();
        auth.action = a;
        auth.defaultPage = s.defaultPage ? (hasDefaultPage = true) && 1 : 0;
        auth.groupv2Id = this.groupv2Id;
        auths.push(auth);
      }
    })));
    if (auths.length && !hasDefaultPage) {
      this.alert.error('請選擇登入頁');
      this.share.loading = false;
      return;
    }
    this.userAuthorityService.saveAuth(this.groupv2Id, auths).subscribe(res => {
      this.queryAuths(this.groupv2Id, '權限儲存完成');
    });
  }

  // 覆核
  checkAuth(s: Submenu) {
    this.alert.clear();
    s.checking = true;
    this.userAuthorityService.checkAuth(this.groupv2Id, s.submenuId).subscribe(res => {
      this.queryAuths(this.groupv2Id, '', s);
    })
  }

  /**
   * Submenu, Action checkbox 點選時，改變相關的 checkbox 狀態
   */
  onChecked() {
    for (var m of this.list) {
      m.checkAll = true;
      for (var s of m.submenus) {
        s.checkAll = true;
        for (var a of s.actions) {
          a.isView && ((a.checked = s.checked) || !s.checked && (s.defaultPage = false));
          !s.checked && (a.checked = false);
          m.checkAll && !a.checked && (m.checkAll = false);
          s.checkAll && !a.checked && (s.checkAll = false);
        }
      }
    }
  }

  /**
   * 勾選 m 底下的 submenus & actions
   * @param m 
   */
  menuCheckAll(m: Menu) {
    let checked = m.checkAll;
    m.submenus.forEach(s => {
      s.checkAll = checked;
      this.submenuCheckAll(s);
    });
  }

  /**
   * 勾選 s 底下的 actions
   * @param s 
   */
  submenuCheckAll(s: Submenu) {
    s.checked = s.checkAll;
    s.actions.forEach(a => a.checked = s.checkAll);
    !s.checked && (s.defaultPage = false);
    this.list.forEach(m => {
      m.checkAll = true;
      if (m.submenus.find(s => !s.checkAll)) {
        m.checkAll = false;
      }
    });
  }

  /**
   * 點選defaultPage：取消其他的defaultPage
   * @param s 
   */
  defaultPageChanged(s: Submenu) {
    s.defaultPage = !s.defaultPage;
    s.defaultPage && this.list.forEach(m => m.submenus.forEach(tempS => tempS !== s && (tempS.defaultPage = false)));
  }

  private queryGroupv2List(self) {
    // 查詢所有群組
    self.groupv2Service.query().subscribe((res) => {
      self.groupv2List = res.list;
      // 查詢指定群組的權限
      self.queryAuths(self.groupv2List[0].groupv2Id);
    });
  }

  /**
   * 清除所有checked, defaultPage
   */
  clearAuthForm() {
    this.list.forEach(m => {
      m.checkAll = false;
      m.submenus.forEach(s => {
        s.checkAll = s.checked = s.defaultPage = false;
        s.auth = undefined;
        s.actions.forEach(a => a.checked = false)
      })
    });
  }

  /**
   * 利用 Auth 列表 (list.groupv2Auths) 改變 Menu 列表 (this.list) 中資料的 checked/checkAll 狀態
   * @param this
   */
  private updateAuthForm(this) {
    for (let menu of this.list) {
      menu.checkAll = true;

      for (let submenu of menu.submenus) {
        submenu.checkAll = true;
        submenu.checked = false;
        submenu.defaultPage = false;
        submenu.auth = undefined;
        let actions = submenu.actions;

        for (let action of submenu.actions) {
          action.checked = false;
          action.isView = action.actionAuthName == submenu.state;

          for (let auth of this.groupv2Auths) {
            if (auth.action.objectId == action.objectId) {
              action.checked = true;
              submenu.checked = true;
              !submenu.auth && (submenu.auth = auth);
              auth.defaultPage && (submenu.defaultPage = true);
              break;
            }
          }

          if (!action.checked) {
            submenu.checkAll = false;
            menu.checkAll = false;
          }
        }
      }
    }
  }

}
