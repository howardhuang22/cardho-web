/**
 * Created by skylee on 2017/7/12.
 */

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import { ShareService } from "app/service/share.service";
import { Groupv2UserService } from "app/api/groupv2-user.service";

import { MemberUser } from './../../model/member-user';
import {MemberCollection} from "../../model/member-collection";
import {MemberCollectionService} from "../../api/memberCollection.service";

@Component({
    selector: 'app-member-collection-management',
    templateUrl: './member-collection-management-component.html',
    styleUrls: ['./member-collection-management-component.css']
})
export class MemberCollectionManagementComponent extends BaseComponent<MemberCollection> {

    searchColumns = 'collectionId, collectionUserId, collectionActivityId, collectionObjectId, isActived, doSearch, add';

    constructor(protected memberCollectionService: MemberCollectionService, protected alert: AlertService, protected base: BaseService,
                protected modalService: NgbModal, public share: ShareService) {
        super(memberCollectionService, alert, base, modalService, share, MemberCollection);
    }

}
