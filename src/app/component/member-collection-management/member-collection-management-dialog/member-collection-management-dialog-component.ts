/**
 * Created by skylee on 2017/7/22.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BaseDialog} from "../../base-dialog";
import {ShareService} from "../../../service/share.service";
import {MemberCollection} from "../../../model/member-collection";
import {MemberTreasure} from "../../../model/member-treasure";
import {MemberActivity} from "../../../model/member-activity";
import {MemberTreasureService} from "../../../api/memberTreasure.service";
import {MemberActivityService} from "../../../api/memberActivity.service";

@Component({
    selector: 'app-member-collection-management-dialog',
    templateUrl: './member-collection-management-dialog-component.html',
    styleUrls: ['./member-collection-management-dialog-component.css']
})
export class MemberCollectionManagementDialogComponent extends BaseDialog<MemberCollection> {

    treasureList: MemberTreasure[] = [];
    activityList: MemberActivity[] = [];

    constructor(public share: ShareService, private memberTreasureService: MemberTreasureService, private  memberActivityService: MemberActivityService) {
        super(share);
    }
    
    ngOnInit() {
        this.memberTreasureService.getList().subscribe(res => this.treasureList = res.list);
        this.memberActivityService.getList().subscribe(res => this.activityList = res.list);
        if (this.item == null) {
            this.item = new MemberActivity();
            this.item.isActived = 1;
        }
    }

    edit(editedItem) {
        this.share.loading = true;
        this.item.isActived = editedItem.isActived;
        this.onEdit.emit(this.item);
    }
}

