import { Groupv2User } from './../../model/groupv2-user';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import { ShareService } from "app/service/share.service";
import { Groupv2UserService } from "app/api/groupv2-user.service";

@Component({
  selector: 'app-groupv2-user-management',
  templateUrl: './groupv2-user-management.component.html',
  styleUrls: ['./groupv2-user-management.component.css']
})
export class Groupv2UserManagementComponent extends BaseComponent<Groupv2User> {

  searchColumns = 'groupv2UserIdLike, groupv2UserName, enabled, checked, searchIndex, add, print, insertDate, modifyDate, insertUser, doSearch';
  searchIndexContent = '使用者代碼';

  constructor(protected groupv2UserService: Groupv2UserService, protected alert: AlertService, protected base: BaseService,
    protected modalService: NgbModal, public share: ShareService) {
    super(groupv2UserService, alert, base, modalService, share, Groupv2User);
  }

}
