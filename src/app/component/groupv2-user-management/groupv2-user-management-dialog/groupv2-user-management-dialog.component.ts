import { Groupv2Service } from './../../../api/groupv2.service';
import { Groupv2User } from './../../../model/groupv2-user';
import { ShareService } from 'app/service/share.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentItem } from './../../../model/current-item';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseDialog } from "app/component/base-dialog";
import { Groupv2 } from "app/model/groupv2";

@Component({
  selector: 'app-groupv2-user-management-dialog',
  templateUrl: './groupv2-user-management-dialog.component.html',
  styleUrls: ['./groupv2-user-management-dialog.component.css']
})
export class Groupv2UserManagementDialogComponent extends BaseDialog<Groupv2User> {

  groupv2List: Groupv2[] = [];

  constructor(public share: ShareService, private groupv2Service: Groupv2Service) {
    super(share);
  }

  ngOnInit() {
    this.groupv2Service.query().subscribe(res => this.groupv2List = res.list);
  }

  add(item: Groupv2User) {
    this.share.loading = true;
    item.groupv2 = this.groupv2List.find(groupv2 => groupv2.groupv2Id == item.groupv2Id);
    this.onAdd.emit(item);
  }

  edit(editedItem) {
    this.share.loading = true;
    editedItem.groupv2 = this.groupv2List.find(groupv2 => groupv2.groupv2Id == editedItem.groupv2Id);
    for (let key in this.item) {
      this.item[key] = editedItem[key] || this.item[key];
    }
    this.onEdit.emit(this.item);
  }
}
