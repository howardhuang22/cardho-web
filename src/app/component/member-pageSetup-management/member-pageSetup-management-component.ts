/**
 * Created by skylee on 2017/7/17.
 */

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import { ShareService } from "app/service/share.service";

import {MemberPageSetup} from "../../model/member-pageSetup";
import {MemberPageSetupService} from "../../api/memberPageSetup.service";

@Component({
    selector: 'app-member-pageSetup-management',
    templateUrl: './member-pageSetup-management-component.html',
    styleUrls: ['./member-pageSetup-management-component.css']
})
export class MemberPageSetupManagementComponent extends BaseComponent<MemberPageSetup> {

    searchColumns = 'memberPageId, memberPageName, enabled, add, doSearch';

    constructor(protected memberPageSetupService: MemberPageSetupService, protected alert: AlertService, protected base: BaseService,
                protected modalService: NgbModal, public share: ShareService) {
        super(memberPageSetupService, alert, base, modalService, share, MemberPageSetup);
    }

}


