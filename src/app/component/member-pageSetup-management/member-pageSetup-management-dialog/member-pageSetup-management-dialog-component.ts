/**
 * Created by skylee on 2017/7/17.
 */

import { ShareService } from 'app/service/share.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentItem } from './../../../model/current-item';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseDialog } from "app/component/base-dialog";
import {MemberPageSetup} from "../../../model/member-pageSetup";

@Component({
    selector: 'app-member-pageSetup-management-dialog',
    templateUrl: './member-pageSetup-management-dialog-component.html',
    styleUrls: ['./member-pageSetup-management-dialog-component.css']
})
export class MemberPageSetupManagementDialogComponent extends BaseDialog<MemberPageSetup> {
    constructor(public share: ShareService) {
        super(share);
    }

    ngOnInit() {
        if (this.item == null) {
            this.item = new MemberPageSetup();
            this.item.enabled = 0;
        }
    }
}
