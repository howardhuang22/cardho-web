/**
 * Created by skylee on 2017/7/12.
 */

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseService } from 'app/service/base.service';
import { AlertService } from 'app/service/alert.service';
import { Component } from '@angular/core';
import { BaseComponent } from "app/component/base-component";
import { ShareService } from "app/service/share.service";
import { Groupv2UserService } from "app/api/groupv2-user.service";

import {MemberTag} from "../../model/member-tag";
import {MemberTagService} from "../../api/memberTag.service";

@Component({
    selector: 'app-member-tag-management',
    templateUrl: './member-tag-management-component.html',
    styleUrls: ['./member-tag-management-component.css']
})
export class MemberTagManagementComponent extends BaseComponent<MemberTag> {

    searchColumns = 'memberTagId, memberTagName, enabled, add, doSearch';

    constructor(protected memberTagService: MemberTagService, protected alert: AlertService, protected base: BaseService,
                protected modalService: NgbModal, public share: ShareService) {
        super(memberTagService, alert, base, modalService, share, MemberTag);
    }

}

