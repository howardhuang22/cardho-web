/**
 * Created by skylee on 2017/7/17.
 */

import { ShareService } from 'app/service/share.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentItem } from './../../../model/current-item';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BaseDialog } from "app/component/base-dialog";
import {MemberTag} from "../../../model/member-tag";

@Component({
    selector: 'app-member-tag-management-dialog',
    templateUrl: './member-tag-management-dialog-component.html',
    styleUrls: ['./member-tag-management-dialog-component.css']
})
export class MemberTagManagementDialogComponent extends BaseDialog<MemberTag> {
    constructor(public share: ShareService) {
        super(share);
    }

    ngOnInit() {
        if (this.item == null) {
            this.item = new MemberTag();
            this.item.enabled = 0;
        }
    }
}

