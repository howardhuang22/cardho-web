import { Menu } from "app/model/menu";

export class Mainmenu {

    mainmenuId: string;
    userType: number;
    name: string;
    ordinal: number;
    menus: Menu[];

    // For auth conf page
    checkAll: boolean;

}
