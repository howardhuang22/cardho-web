import { User } from 'app/model/user';

export class Groupv2 {

    groupv2Id: string;
    groupv2Name: string;
    adminUser: User;
    enableFlag: string;
    insertUser: string;
    insertDate : Date;
    modifyUser: string;
    modifyDate : Date;
    checkUser: string;
    checkDate : Date;
    deleteUser: string;
    deleteDate : Date;

}
