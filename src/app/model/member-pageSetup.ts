/**
 * Created by skylee on 2017/7/17.
 */

export class MemberPageSetup {
    id : string;
    name : string;
    creatorUserId : string;
    updateTime : string;
    updateUserId : string;
    enabled : number;
}
