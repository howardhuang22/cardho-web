import { Action } from './action';
export class UserAuthority {
    
    objectId: string;
    groupv2Id: string;
    defaultPage: number;
    action: Action;
    insertUser: string;
    insertDate: Date;
    modifyUser: string;
    modifyDate: Date;
    checkUser: string;
    checkDate: Date;
    
}
