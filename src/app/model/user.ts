export class User {

    userId: string;
    userName: string;
    userType: string;
    enabled: number;

    constructor(obj: any) {
        this.userId = obj.userId;
        this.userName = obj.userName;
        this.userType = obj.userType;
    }
}
