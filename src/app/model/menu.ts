import { Submenu } from './submenu';
export class Menu {

    menuId: string;
    mainmenuId: string;
    userType: number;
    name: string;
    ordinal: number;
    submenus: Submenu[];

    // For auth conf page
    checkAll: boolean;

}
