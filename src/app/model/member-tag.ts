/**
 * Created by skylee on 2017/7/17.
 */

export class MemberTag {
    id : string;
    name : string;
    parentTagId : string;
    orderIndex : number;
    creatorUserId : string;
    createTime : string;
    updateTime : string;
    updateUserId : string;
    enabled : number;
}
