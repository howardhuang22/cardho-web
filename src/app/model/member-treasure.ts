/**
 * Created by skylee on 2017/7/18.
 */

export class MemberTreasure {
    id : string;
    name : string;
    createTime : string;
    type : string;
    file : string;
    code : string;
    url : string;
    creatorUserId : string;
    updateTime : string;
    updateUserId : string;
    enabled : number;
    pageId : string;
    index : number;
    pageSetup : string;
}