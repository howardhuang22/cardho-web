export class BillHistory {

    id: string;
    podetailId: string;
    poType: string;
    billStatus: string;
    thisBillAmount: number;
    totalBillAmount: number;
    billCnt: number;
    billFile: string;
    thisCardFee: number;
    totalCardFee: number;
    billDate: Date;
    specFlag: string;

}
