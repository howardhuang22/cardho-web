/**
 * Created by skylee on 2017/7/18.
 */

export class MemberActivity {
    id : string;
    url : string;
    description : string;
    startTime : string;
    endTime : string;
    condition : string;
    isActived : number;
    pageId : string;
    objectId : string;
    index : number;
}