import { UserAuthority } from './user-authority';
import { Action } from './action';
export class Submenu {
    
    mainmenuId: string;
    menuId: string;
    submenuId: string;
    name: string;
    ordinal: number;
    state: string;
    userType: string;
    actions: Action[];

    // For auth conf page
    checked: boolean;
    checkAll: boolean;
    defaultPage: boolean;
    auth: UserAuthority;
    checking: boolean;

}
