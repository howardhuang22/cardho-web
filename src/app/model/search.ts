import { Groupv2 } from './groupv2';
export class Search {

    id: string;
    
    // member user
    account: string;
    displayName: string;
    type : string;
    signType : string;
    
    // tag
    name: string;
    
    // collection box

    // pageSetup
    // name
    
    // treasure object
    index : number;
    pageId : string;
    code: string;
    
    // activity
    objectId : string;

    isActived : number;
    enabled : number;
    
    // 分頁
    page : number;
}
