/**
 * Created by skylee on 2017/7/5.
 */

export class MemberUser {
    id : string;
    account : string;
    password : string;
    displayName : string;
    enabled : number;
    signType : string;
    type : string;
}
