import { Page } from 'app/model/page';
export class ApiRes {
    list?: any[];
    page?: Page;
    item?: any;
    isList?: boolean;
}
