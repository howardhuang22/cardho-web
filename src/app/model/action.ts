export class Action {

    objectId: string;
    userType: string;
    mainmenuId: string;
    menuId: string;
    submenuId: string;
    actionId: string;
    actionDesc: string;
    actionAuthName: string;
    ordinal: number;
    enabled: number;

    // For auth conf page
    checked: boolean;
    isView: boolean;

}
