export class BatchProcessStatus {
    
    batchId: string;
    batchFile: string;  // 檔名
    insertRecord: number;
    updateRecord: number;
    deleteRecord: number;
    invalidRecord: number;
    failedRecord: number;
    totalRecord: number;
    logFile: string;  // 路徑(與log無關)
    status: string;
    batchType: string;
    insertUser: string;
    insertDate: Date;
    finishDate: Date;
    statusDesc: string;

}
