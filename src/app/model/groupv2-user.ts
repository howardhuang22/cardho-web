import { Groupv2 } from './groupv2';
import { User } from 'app/model/user';

export class Groupv2User {

    objectId: string;
    groupv2: Groupv2;
    user: User;
    insertUser: string;
    insertDate: Date;
    modifyUser: string;
    modifyDate: Date;
    checkUser: string;
    checkDate: Date;
    deleteUser: string;
    deleteDate: Date;
    description: string;
    lastLoginDate: Date;

    // Use on page
    groupv2Id: string;

}
