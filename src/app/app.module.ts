

import { MainmenuService } from 'app/api/mainmenu.service';
import { LpdmService } from './api/lpdm.service';
import { Groupv2UserManagementDialogComponent } from './component/groupv2-user-management/groupv2-user-management-dialog/groupv2-user-management-dialog.component';
import { Groupv2ManagementDialogComponent } from './component/groupv2-management/groupv2-management-dialog/groupv2-management-dialog.component';

import { BatchProcessStatusService } from './api/batch-process-status.service';

import { UserAuthorityService } from './api/user-authority.service';

import { SubmenuService } from './api/submenu.service';

import { Groupv2UserService } from './api/groupv2-user.service';
import { Groupv2Service } from './api/groupv2.service';

import { ShareService } from './service/share.service';
import { NgbModule, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from './service/api.service';

import { PaginationComponent } from './component/common/pagination/pagination.component';
import { MenuService } from 'app/api/menu.service';
import { AuthGuard } from './guard/auth.guard';
import { routing } from './app.routing';
import { RestService } from './service/rest.service';
import { BaseService } from 'app/service/base.service';
import { AlertService } from './service/alert.service';
import { AuthService } from './service/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/common/header/header.component';
import { FooterComponent } from './component/common/footer/footer.component';
import { SidebarComponent } from './component/common/sidebar/sidebar.component';
import { MainComponent } from "app/component/common/main/main.component";
import { SearchFormComponent } from './component/common/search-form/search-form.component';

import { Groupv2ManagementComponent } from './component/groupv2-management/groupv2-management.component';
import { Groupv2UserManagementComponent } from './component/groupv2-user-management/groupv2-user-management.component';
import { Groupv2UserAuthConfComponent } from './component/groupv2-user-auth-conf/groupv2-user-auth-conf.component';

import { MomentModule } from 'angular2-moment';
import { LoadingComponent } from './component/common/loading/loading.component';
import { BeginEndDatepickerComponent } from './component/common/begin-end-datepicker/begin-end-datepicker.component';

import { DateUtilsService } from "app/service/date-util.service";
import { I18n } from "app/customization/i18n";
import { PageErrorHandler } from "app/customization/page-error-handler";
import { CustomDateParserFormatter } from "app/customization/custom-date-parser-formatter";
import { CustomDatepickerI18n } from "app/customization/custom-datepicker-i18n";
import { AlertComponent } from "app/component/common/alert/alert.component";
import { LoginComponent } from "./component/login-management/login-management-component";

import { FileDropDirective, FileSelectDirective } from 'ng2-file-upload';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

// MEMBER USER
import { MemberUserManagementComponent } from './component/member-user-management/member-user-management-component';
import { MemberUserManagementDialogComponent } from './component/member-user-management/member-user-management-dialog/member-user-management-dialog-component';
import { MemberTagManagementComponent } from './component/member-tag-management/member-tag-management-component';
import { MemberTagManagementDialogComponent } from './component/member-tag-management/member-tag-management-dialog/member-tag-management-dialog-component';
import { MemberCollectionManagementComponent } from './component/member-collection-management/member-collection-management-component';
import { MemberCollectionManagementDialogComponent } from './component/member-collection-management/member-collection-management-dialog/member-collection-management-dialog-component';
import { MemberActivityManagementComponent } from './component/member-activity-management/member-activity-management-component';
import { MemberActivityManagementDialogComponent } from "./component/member-activity-management/member-activity-management-dialog/member-activity-management-dialog-component";
import { MemberTreasureManagementComponent } from './component/member-treasure-management/member-treasure-management-component';
import { MemberTresureManagementDialogComponent } from "./component/member-treasure-management/member-treasure-management-dialog/member-treasure-management-dialog-component";
import { MemberPageSetupManagementComponent } from './component/member-pageSetup-management/member-pageSetup-management-component';
import { MemberPageSetupManagementDialogComponent } from './component/member-pageSetup-management/member-pageSetup-management-dialog/member-pageSetup-management-dialog-component';

import { DatepickerComponent} from './component/common/datepicker/datepicker.component';

// MEMBEER USER SERVICE
import { MemberUserService } from "./api/memberUser.service";
import { MemberTagService } from "./api/memberTag.service";
import { MemberPageSetupService } from "./api/memberPageSetup.service";
import { MemberCollectionService } from "./api/memberCollection.service";
import { MemberActivityService } from "./api/memberActivity.service";
import { MemberTreasureService } from "./api/memberTreasure.service";

export function httpFactory() {
  return new CustomDateParserFormatter("yyyy/MM/dd");
}

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    AlertComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    LoadingComponent,

    SearchFormComponent,
    PaginationComponent,
    // ProgramInquireComponent,
    Groupv2ManagementComponent,
    Groupv2ManagementDialogComponent,
    Groupv2UserManagementComponent,
    Groupv2UserManagementDialogComponent,
    Groupv2UserAuthConfComponent,

    BeginEndDatepickerComponent,

    // MEMBER USER
    MemberUserManagementComponent,
    MemberUserManagementDialogComponent,
    MemberTagManagementComponent,
    MemberTagManagementDialogComponent,
    MemberCollectionManagementComponent,
    MemberCollectionManagementDialogComponent,
    MemberActivityManagementComponent,
    MemberActivityManagementDialogComponent,
    MemberTreasureManagementComponent,
    MemberTresureManagementDialogComponent,
    MemberPageSetupManagementComponent,
    MemberPageSetupManagementDialogComponent,
    DatepickerComponent,

    FileDropDirective,
    FileSelectDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    NgbModule.forRoot(),
    MomentModule
  ],
  providers: [
    AuthService,
    AlertService,
    BaseService,
    RestService,
    AuthGuard,
    MenuService,

    ApiService,
    { provide: ErrorHandler, useClass: PageErrorHandler },
    ShareService,
    Groupv2Service,
    Groupv2UserService,
    SubmenuService,
    UserAuthorityService,
    
    UserAuthorityService,
    BatchProcessStatusService,
    DateUtilsService,
    I18n,
    { provide: NgbDateParserFormatter, useFactory: httpFactory }, // 自訂 Date 欄位格式為 yyyy/MM/dd,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }, // 自訂 Datepicker 中文 月份/星期 名稱
    LpdmService,
    MainmenuService,

    {provide: LocationStrategy, useClass: HashLocationStrategy},
      
      // MEMBER USER SERVICE
    MemberUserService,
    MemberTagService,
    MemberPageSetupService,
    MemberCollectionService,
    MemberActivityService,
    MemberTreasureService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
