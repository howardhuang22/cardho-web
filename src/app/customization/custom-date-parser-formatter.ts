import { NgbDateParserFormatter, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { DatePipe } from "@angular/common";

export class CustomDateParserFormatter extends NgbDateParserFormatter {

    constructor(private customFormat: string) {
        super();
    };

    format(date: NgbDateStruct): string {
        if (date === null) {
            return '';
        }
        let datePipe = new DatePipe('en-US');
        return datePipe.transform(date, this.customFormat);
    }

    parse(value: string): NgbDateStruct {
        if (!value) {
            return null;
        }
        let d = new Date(value);
        return d ? {
            year: d.getFullYear(),
            month: d.getMonth() + 1,
            day: d.getDate()
        } : null;
    }
}