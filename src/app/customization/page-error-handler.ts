import { ShareService } from 'app/service/share.service';
import { AlertService } from 'app/service/alert.service';
import { Response } from '@angular/http';
import { ApiService } from 'app/service/api.service';
import { Injectable, ErrorHandler, Inject, Injector } from '@angular/core';

export interface LoggingErrorHandlerOptions {
    rethrowError: boolean;
    unwrapError: boolean;
}

export var LOGGING_ERROR_HANDLER_OPTIONS: LoggingErrorHandlerOptions = {
    rethrowError: false,
    unwrapError: false
};

@Injectable()
export class PageErrorHandler implements ErrorHandler {

    constructor(private injector: Injector) {

    }

    public get alert(): AlertService {
        return this.injector.get(AlertService);
    }

    public get share(): ShareService {
        return this.injector.get(ShareService);
    }

    handleError(error) {

        console.error('Error catched:', error);

        this.share.loading = false;
        this.share.printing = false;
        this.share.closeDialog();

        try {

            if (error instanceof Response) {
                console.log('Error Response:',error.json().message);
                this.alert.error(error.json().message);
            }

            console.group("ErrorHandler");
            console.error(error.message);
            console.error(error.stack);
            console.groupEnd();

            // this.alert.error(error.message);

        } catch (handlingError) {
            console.group("ErrorHandler");
            console.warn("Error when trying to output error.");
            console.error(handlingError);
            console.groupEnd();
        }
    }

}
// I am the collection of providers used for this service at the module level.
// Notice that we are overriding the CORE ErrorHandler with our own class definition.
// --
// CAUTION: These are at the BOTTOM of the file so that we don't have to worry about
// creating futureRef() and hoisting behavior.
export var LOGGING_ERROR_HANDLER_PROVIDERS = [
    {
        provide: LOGGING_ERROR_HANDLER_OPTIONS,
        useValue: LOGGING_ERROR_HANDLER_OPTIONS
    },
    {
        provide: ErrorHandler,
        useClass: PageErrorHandler
    }
];
