import { ApiRes } from './../model/api-res';
import { ShareService } from 'app/service/share.service';
import { Response, Http, Headers, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from "app/service/base.service";
import * as FileSaver from 'file-saver';

@Injectable()
export class RestService {

  constructor(private http: Http, private base: BaseService, public share: ShareService) {
  }

  get(url: string, params?: {}) {
    return this.http.get(this.baseUrl + url, {
      headers: this.getHeaders(),
      search: this.transToUrlParams(params)
    }).map((res: Response) => this.transResp(res));
  }

  postFormData(url: string, body: any) {
    let formBody = new FormData();
    formBody.append("reqString", JSON.stringify(body.bodyJson));
    if (body.file) {
      formBody.append("file", body.file, body.file.name);  
    }
    const headers = new Headers();
    headers.append('x-auth-token', this.base.token);

    return this.http.post(this.base.baseUrl + url, formBody, new RequestOptions({ headers: headers }))
        .map((res: Response) => this.transResp(res));
  }
  
  post(url: string, body: any) {
    return this.http.post(this.base.baseUrl + url, JSON.stringify(body), { headers: this.getHeaders() })
      .map((res: Response) => this.transResp(res));
  }

  putFormData(url: string, body: any) {
    let formBody = new FormData();
    formBody.append("reqString", JSON.stringify(body.bodyJson));
    if (body.file) {
      formBody.append("file", body.file, body.file.name);
    }
    const headers = new Headers();
    headers.append('x-auth-token', this.base.token);

    return this.http.put(this.base.baseUrl + url, formBody, new RequestOptions({ headers: headers }))
        .map((res: Response) => this.transResp(res));
  }

  put(url: string, body: any) {
    return this.http.put(this.base.baseUrl + url, JSON.stringify(body), { headers: this.getHeaders() })
      .map((res: Response) => this.transResp(res));
  }

  delete(url: string) {
    return this.http.delete(this.base.baseUrl + url, { headers: this.getHeaders() })
      .map((res: Response) => this.transResp(res));
  }

  print(url: string, body: any, fileName: string) {  //'vnd.ms-excel' application/vnd_ms-excel'
    let blobType = fileName.indexOf('xls') > -1 ? 'vnd_ms-excel' : 'pdf';
    let headers = this.getHeaders();
    headers.append('accept', 'application/' + blobType);
    return this.http.post(this.base.baseUrl + url, JSON.stringify(body), { headers: headers, responseType: ResponseContentType.Blob })
      .map((res: Response) => res.blob())
      .subscribe(
      data => {
        var blob = new Blob([data], { type: 'application/' + blobType });
        FileSaver.saveAs(blob, fileName);
        this.share.printing = false;
      }
      );
  }

  private transResp(response: Response): ApiRes | any {
    let res = response.text() && response.json();
    if (res.page || res._embedded) {
      return <ApiRes>{
        list: res._embedded && res._embedded[Object.keys(res._embedded)[0]] || [],
        page: res.page,
        isList: true
      };
    } else if (res.content) {
      return <ApiRes>{
        list: res.content || [],
        page: res,
        isList: true
      };
    } else {
      return res;
    }
  }

  private getHeaders() {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json;charset=UTF-8');
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('x-auth-token', this.base.token);
    return headers;
  }

  private transToUrlParams(params: {}) {
    let urlParams: URLSearchParams = new URLSearchParams();
    for (var key in params) {
      if (params[key]) {
        urlParams.set(key, params[key]);
      }
    }
    return urlParams;
  }

  get baseUrl() {
    return this.base.baseUrl;
  }

}
