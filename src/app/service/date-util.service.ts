import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

@Injectable()
export class DateUtilsService {

  datePipe = new DatePipe('en-US');  // en-US, zh-TW

  constructor() { }

  transYmd(date: Date): string {
    return this.datePipe.transform(date, 'yyyyMMdd');
  }

  ngbDateToDate(ngbDate: NgbDateStruct): Date {
    return ngbDate && new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
  }

  dateToNgbDate(date: Date): NgbDateStruct {
    typeof date === 'string' && (date = new Date(date));
    return date && <NgbDateStruct>{ year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
  }

  todayDate(): Date {
    let now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate());
  }

}
