import { Search } from './../model/search';
import { RestService } from './rest.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {

  constructor(protected rest: RestService, protected serviceUrl: string) {
  }

  query(search = new Search()) {
    // return this.rest.post(this.serviceUrl + '/queryByObject', search);
    // return this.rest.get(this.serviceUrl, search);
    return this.rest.get(this.serviceUrl + '/search/findByObject', search);
  }

  queryById(id) {
    let search = new Search();
    search.id = id;
    return this.query(search);
  }

  updateById(id, body) {
    return this.rest.put(this.serviceUrl + '/' + id, body);
  }

  saveFormData(body) {
    return this.rest.postFormData(this.serviceUrl, body);
  }

  save(body) {
    return this.rest.post(this.serviceUrl, body);
  }

  updateFormData(id, body) {
    return this.rest.putFormData(this.serviceUrl+ '/' + id, body);
  }

  update(body) {
    return this.rest.put(this.serviceUrl, body);
  }

  remove(id) {
    return this.rest.delete(this.serviceUrl + '/' + id);
  }

  check(id) {
    return this.rest.get(this.serviceUrl + '/check/' + id);
  }

  unlock(id) {
    return this.rest.get(this.serviceUrl + '/unlock/' + id);
  }

  recover(id) {
    return this.rest.get(this.serviceUrl + '/recover/' + id);
  }

  print(search: Search, fileName: string) {
    return this.rest.print(this.serviceUrl + '/print', search, fileName);
  }

  execute(id) {
    return this.rest.get(this.serviceUrl + '/execute/' + id);
  }

  listFiles(id) {
    return this.rest.get(this.serviceUrl + '/listFiles/' + id);
  }

}
