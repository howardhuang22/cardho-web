import { BaseService } from './base.service';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthService {

  constructor(private http: Http, private base: BaseService) {
  }

  /*
   * 取得帳密之後登入系統
   */
  login(username: string, password: string) {


    var body = 'username=' + username + '&password=' + password;
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.base.baseUrl + '/login', body, { headers: headers })
    .map((response: Response) => {
      this.base.user = response.json().user;
      this.base.token = response.headers.get('x-auth-token') || '';
      // this.base.auths = response.json().authorities;
      this.base.auths = "[memberUserManagement, memberUserManagement.append, memberUserManagement.edit," +
                          "memberTagManagement, memberTagManagement.append, memberTagManagement.edit," +
                          "memberCollectionManagementComponent, memberCollectionManagementComponent.append, memberCollectionManagementComponent.edit" +
                          "memberPageSetupManagement, memberPageSetupManagement.append, memberPageSetupManagement.edit," +
                          "memberTreasureManagement, memberTreasureManagement.append, memberTreasureManagement.edit," +
                          "memberActivityManagement, memberActivityManagement.append, memberActivityManagement.edit]";
      return response.json();
    });

    //
    // return this.http.get(this.base.baseUrl + '/src/app/api/responseJson/login.json')
    //   .map((response: Response) => {
    //     this.base.user = response.json().user;
    //     // this.base.token = response.headers.get('x-auth-token') || '';
    //     this.base.token = 'a64fbc01-fdf2-4f95-ba40-6f55e847c6e4';
    //     this.base.auths = response.json().authorities;
    //     return response.json();
    //   });
  }


  logout() {
  }

}
