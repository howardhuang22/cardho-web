import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { User } from "app/model/user";
import { Http } from "@angular/http";

@Injectable()
export class BaseService {

  private _baseUrl: string;
  private _token: string;
  private _user: User;
  private _auths: string;

  constructor(public http: Http) {
    console.info((environment.production ? '[prod]' : '[dev]'), environment.apiBaseUrl);
    this.baseUrl = environment.apiBaseUrl;
  }

  // Getter / Setter

  get baseUrl() {
    return this._baseUrl || localStorage['baseUrl'] || '';
  }

  set baseUrl(baseUrl: string) {
    this._baseUrl = baseUrl;
    localStorage['baseUrl'] = baseUrl;
  }

  get token() {
    return this._token || localStorage['token'] || '';
  }

  set token(token: string) {
    this._token = token.toString();
    localStorage['token'] = token;
  }

  get user() {
    return this._user || JSON.parse(localStorage['user'] || '{}');
  }

  set user(user: User) {
    this._user = user;
    localStorage['user'] = JSON.stringify(user);
  }

  get auths() {
    return this._auths || JSON.parse(localStorage['auths'] || '{}');
  }

  set auths(auths: string) {
    this._auths = auths;
    localStorage['auths'] = JSON.stringify(auths);
  }

}
