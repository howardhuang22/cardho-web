import { NavigationStart } from '@angular/router';
import { Router, NavigationEnd } from '@angular/router';
import { Search } from './../model/search';
import { NgbModalRef, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { Page } from "app/model/page";

@Injectable()
export class ShareService {

  private _loading: boolean;
  private _printing: boolean;
  private _executing: boolean;
  private _dialog: NgbModalRef;
  private _search: Search;
  private _page: Page;
  private _checking: boolean;
  private _currentUrl: string;
  private _openeddp: NgbInputDatepicker; // 現在被打開的 datepicker

  constructor(private tmpRouter: Router) { 
    this.subscribeRouterChange(tmpRouter);
  }

  clear() {
    this.dialog = null;
    this.openeddp = null;
    this.loading = false;
    this.printing = false;
    this.executing = false;
    this.page = new Page();
    this.search = new Search();
  }

  subscribeRouterChange(tmpRouter: Router) {
    tmpRouter.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.clear();
      } else if (event instanceof NavigationEnd) {
        this.currentUrl = event.url.substring(6);
      }
    });
  }

  closeDialog() {
    this.dialog && this.dialog.close();
  }

  json(str) {
    return JSON.stringify(str);
  }

  get loading() {
    return this._loading;
  }

  set loading(loading: boolean) {
    this._loading = loading;
  }

  get openeddp() {
    return this._openeddp;
  }

  set openeddp(openeddp: NgbInputDatepicker) {
    this._openeddp = openeddp;
  }

  get printing() {
    return this._printing;
  }

  set printing(printing: boolean) {
    this._printing = printing;
  }

  get executing() {
    return this._executing;
  }

  set executing(executing: boolean) {
    this._executing = executing;
  }

  get checking() {
    return this._checking;
  }

  set checking(checking: boolean) {
    this._checking = checking;
  }

  get dialog() {
    return this._dialog;
  }

  set dialog(dialog: NgbModalRef) {
    this._dialog = dialog;
  }

  get search() {
    return this._search || (this._search = new Search());
  }

  set search(search: Search) {
    this._search = search;
  }

  get page() {
    return this._page || (this._page = new Page());
  }

  set page(page: Page) {
    this._page = page;
  }

  get currentUrl() {
    return this._currentUrl;
  }

  set currentUrl(currentUrl: string) {
    this._currentUrl = currentUrl;
  }

  /* 共用的樣式 */
  public style = {
    dfg:  'form-group ml-2 mb-3 row',  // dialog form-group
    dfgr: 'form-group ml-2 mb-3 row required', // dialog form-group with required
    dl: 'col-md-3 col-form-label form-control-sm', // dialog label
    dib: 'col-md-7', // dialog input box
    di: 'form-control form-control-sm', // dialog input
    dsb: 'btn btn-outline-primary btn-sm', // dialog save button
    dcb: 'btn btn-outline-secondary btn-sm', // dialog close button
  };

}
