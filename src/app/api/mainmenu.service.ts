import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";

@Injectable()
export class MainmenuService extends ApiService {

  constructor(protected rest: RestService) {
    super(rest, '/MainmenuService');
  }

  // 依據使用者目前的token取得功能列表
  findByUserInSession() {
    return this.rest.get(this.serviceUrl + '/findByUserInSession');
  }

  // 依據使用者類別取得功能列表
  findByUserType(type: string) {
    return this.rest.get(this.serviceUrl + '/findByUserInSession', { userType: type });
  }

  // For demo only
  query() {
    return this.rest.get('/src/app/api/MenuService_query.json');
  }

}
