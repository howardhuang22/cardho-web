import { ApiService } from 'app/service/api.service';
import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";

@Injectable()
export class SubmenuService extends ApiService {

  constructor(protected rest: RestService) {
    super(rest, '/SubmenuService');
  }

}
