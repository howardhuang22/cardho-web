/**
 * Created by skylee on 2017/7/17.
 */

import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";

@Injectable()
export class MemberPageSetupService extends ApiService {

    constructor(protected rest: RestService) {
        super(rest, '/rest/PageSetup');
    }

    getList() {
        return this.rest.get(this.serviceUrl);
    }
}
