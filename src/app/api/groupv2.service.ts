import { ApiService } from './../service/api.service';
import { RestService } from './../service/rest.service';
import { Injectable } from '@angular/core';

@Injectable()
export class Groupv2Service extends ApiService {

  constructor(protected rest: RestService) {
    super(rest, '/Groupv2Service');
  }

  
  // For demo only
  query() {
    return this.rest.get('/src/app/api/Groupv2Service_query.json');
  }

}
