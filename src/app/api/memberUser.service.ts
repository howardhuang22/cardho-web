/**
 * Created by skylee on 2017/7/14.
 */

import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";

@Injectable()
export class MemberUserService extends ApiService {

    constructor(protected rest: RestService) {
        super(rest, '/rest/MemberUser');
    }
    
    // updateUser(id, body) {
    //     return this.rest.p
    // }
    
}