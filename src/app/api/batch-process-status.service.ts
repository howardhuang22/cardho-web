import { ApiService } from './../service/api.service';
import { RestService } from './../service/rest.service';
import { Injectable } from '@angular/core';

@Injectable()
export class BatchProcessStatusService extends ApiService {

  constructor(protected rest: RestService) {
    super(rest, '/BatchProcessStatusService');
  }

  execute(id) {
    return this.rest.get(this.serviceUrl + '/reimport/' + id);
  }

}
