import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";

@Injectable()
export class MenuService extends ApiService {

  constructor(protected rest: RestService, private http: Http) {
    super(rest, '/MenuService');
  }

  // 依據使用者目前的token取得功能列表
  findByUserInSession() {
    return this.rest.get(this.serviceUrl + '/findByUserInSession');
    // return this.rest.get('/src/app/api/responseJson/MenuService_findByUserInSessionCaho.json');
  }

  // 依據使用者類別取得功能列表
  findByUserType(type: string) {
    return this.rest.get(this.serviceUrl + '/findByUserInSession', { userType: type });
  }

}
