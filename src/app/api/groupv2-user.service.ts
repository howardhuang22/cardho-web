import { ApiService } from './../service/api.service';
import { RestService } from './../service/rest.service';
import { Injectable } from '@angular/core';

@Injectable()
export class Groupv2UserService extends ApiService {

  constructor(protected rest: RestService) {
    super(rest, '/Groupv2UserService');
  }

  // For demo only
  query() {
    return this.rest.get('/src/app/api/Groupv2UserService_query.json');
  }

}
