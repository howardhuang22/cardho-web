import { UserAuthority } from './../model/user-authority';
import { ApiService } from './../service/api.service';
import { RestService } from './../service/rest.service';
import { Injectable } from '@angular/core';

@Injectable()
export class UserAuthorityService extends ApiService {

  constructor(protected rest: RestService) {
    super(rest, '/UserAuthService');
  }

  queryByGroupv2Id(groupv2Id) {
    // return this.rest.get(this.serviceUrl + '/findByGroupv2Id', { groupv2Id: groupv2Id });
    return this.rest.get('/src/app/api/UserAuthService_findByGroupv2Id.json');
    
  }

  saveAuth(groupv2Id, data: UserAuthority[]) {
    return this.rest.put(this.serviceUrl + '/batchSave/' + groupv2Id, { auths: data });
  }

  checkAuth(groupv2Id, submenuId) {
    return this.rest.get(this.serviceUrl + '/check', { groupv2Id: groupv2Id, submenuId: submenuId });
  }

}
