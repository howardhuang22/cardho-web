/**
 * Created by skylee on 2017/7/18.
 */

import { Http, Headers, Response , ResponseContentType, RequestOptions} from '@angular/http';
import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";
import {MemberTreasure} from "../model/member-treasure";
import {BaseService} from "../service/base.service";
import { FileUploader } from 'ng2-file-upload';
import { saveAs } from "file-saver";
import {Observable} from 'rxjs/Rx';
// import { ResponseContentType } from '@angular/http';

@Injectable()
export class MemberTreasureService extends ApiService {

    constructor(protected rest: RestService, private http: Http, private base:BaseService) {
        super(rest, '/rest/TreasureObject');
    }

    getList() {
        return this.rest.get(this.serviceUrl);
    }

    download(item: MemberTreasure) { //get file from service
        const headers = new Headers();
        headers.append('x-auth-token', this.base.token);
        // headers.append('Access-Control-Request-Headers', '*');
        this.http.get(this.rest.baseUrl + '/rest/TreasureObject/' + item.id + '/file', {
            // method: RequestMethod.Post,
            responseType: ResponseContentType.Blob,
            headers: headers//new Headers({'Content-Type', 'application/x-www-form-urlencoded'})
        }).subscribe(
            (response) => { // download file
                var headers = response.headers;
                console.log(headers.getAll);
                // console.log(headers.get('content-disposition'));
                var contentType = response.headers.get('content-type');
                var contentDisposition = response.headers.get('content-disposition');
                var indexFile = contentDisposition.indexOf('filename="');
                var fileName = contentDisposition.substring(indexFile + 10, contentDisposition.length - 1);

                var blob = new Blob([response.blob()], {type: contentType});
                // var blob = new Blob([response.arrayBuffer()], {type: contentType});
                // var filename = 'file.pdf';

                saveAs(blob, fileName);
            });
    }

    // download(item: MemberTreasure): Observable<Object[]> {
    //     return Observable.create(observer => {
    //         let xhr = new XMLHttpRequest();
    //         xhr.open('GET', this.rest.baseUrl + '/rest/TreasureObject/' + item.id + '/file', true);
    //         xhr.setRequestHeader('Content-type', 'application/json');
    //         xhr.setRequestHeader('x-auth-token', this.base.token);
    //         // xhr.responseType='blob';
    //
    //         xhr.onreadystatechange = function () {
    //             if (xhr.readyState === 4) {
    //                 if (xhr.status === 200) {
    //                     console.log(xhr.response);
    //                     var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    //                     var blob = new Blob([xhr.response], { type: contentType });
    //                     observer.next(blob);
    //                     observer.complete();
    //                 } else {
    //                     observer.error(xhr.response);
    //                 }
    //             }
    //         }
    //         xhr.send();
    //
    //     });
    // }

    // saveFromData(item: MemberTreasure, file : File) {
    //     let formBody = new FormData();
    //     formBody.append("reqString", JSON.stringify(item));
    //     formBody.append("file", file, file.name);
    //    
    //     const headers = new Headers();
    //     headers.append('x-auth-token', this.base.token);
    //     return this.http.post(this.rest.baseUrl + '/rest/TreasureObject', formBody, new RequestOptions({ headers: headers }))
    //         .map((response: Response) => {
    //             response.json();
    //     });
    // }
    //
    //
    // updateFromData(item: MemberTreasure) {
    //
    //     let formBody = new FormData();
    //     formBody.append("reqString", JSON.stringify(item));
    //     formBody.append("file", null, '');
    //
    //     const headers = new Headers();
    //     headers.append('x-auth-token', this.base.token);
    //     return this.http.put(this.rest.baseUrl + '/rest/TreasureObject/' + item.id, formBody, new RequestOptions({ headers: headers }))
    //         .map((response: Response) => {
    //             response.json();
    //         });
    // }
}