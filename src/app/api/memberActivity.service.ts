/**
 * Created by skylee on 2017/7/18.
 */

import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";

@Injectable()
export class MemberActivityService extends ApiService {

    constructor(protected rest: RestService) {
        super(rest, '/rest/TreasureActivity');
    }

    getList() {
        return this.rest.get(this.serviceUrl);
    }
}
