/**
 * Created by skylee on 2017/7/17.
 */

import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";

@Injectable()
export class MemberTagService extends ApiService {

    constructor(protected rest: RestService) {
        super(rest, '/rest/MemberTag');
    }
    
    // editTag (id) {
    //     return this.rest.put('membertag/' + id);
    // }
}
