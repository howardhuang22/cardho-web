import { Injectable } from '@angular/core';
import { RestService } from "app/service/rest.service";
import { ApiService } from "app/service/api.service";

@Injectable()
export class LpdmService extends ApiService {

  constructor(protected rest: RestService) {
    super(rest, '/LPDMDataService');
  }

  log(id, scrnFunction: string) {
    return this.rest.get(this.serviceUrl + '/log/' + id, { scrnFunction: scrnFunction });
  }

}
