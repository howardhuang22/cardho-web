import { RouterModule, Routes, Route } from '@angular/router';
import { MainComponent } from './component/common/main/main.component';
import { AuthGuard } from './guard/auth.guard';
import { FooterComponent } from './component/common/footer/footer.component';
import { SidebarComponent } from './component/common/sidebar/sidebar.component';
import { HeaderComponent } from './component/common/header/header.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from "./component/login-management/login-management-component";

import { Groupv2UserManagementComponent } from './component/groupv2-user-management/groupv2-user-management.component';
import { Groupv2UserAuthConfComponent } from './component/groupv2-user-auth-conf/groupv2-user-auth-conf.component';
import { Groupv2ManagementComponent } from './component/groupv2-management/groupv2-management.component';

// MEMBER MANAGEMENT
import { MemberUserManagementComponent } from  './component/member-user-management/member-user-management-component';
import { MemberTagManagementComponent } from  './component/member-tag-management/member-tag-management-component';
import { MemberPageSetupManagementComponent } from  './component/member-pageSetup-management/member-pageSetup-management-component';
import { MemberCollectionManagementComponent } from './component/member-collection-management/member-collection-management-component';
import { MemberTreasureManagementComponent } from "./component/member-treasure-management/member-treasure-management-component";
import { MemberActivityManagementComponent } from "./component/member-activity-management/member-activity-management-component";

const appRoutes: Routes = [
  { path: 'login', redirectTo: 'loginComponent'},
  // { path: 'index.html', redirectTo: 'loginComponent', pathMatch: 'full' },
  { path: '', redirectTo: 'loginComponent', pathMatch: 'full' },
  { path: 'loginComponent', component: LoginComponent },
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuard],
    // canActivateChild: [AuthGuard],
    children: [
        // SYSTEM USER
      { path: 'groupv2Management', component: Groupv2ManagementComponent },
      { path: 'groupv2UserAuthConf', component: Groupv2UserAuthConfComponent },
      { path: 'groupv2UserManagement', component: Groupv2UserManagementComponent },
        
        // MEMBER MANAGEMENT
      { path: 'memberUserManagement', component: MemberUserManagementComponent},
      { path: 'memberUserTagManagement', component: MemberTagManagementComponent},
      { path: 'memberCollectionManagementComponent', component: MemberCollectionManagementComponent},
      { path: 'memberPageSetupManagement', component: MemberPageSetupManagementComponent },
      { path: 'memberActivityManagement', component: MemberActivityManagementComponent },
      { path: 'memberTreasureManagement', component: MemberTreasureManagementComponent },
    ]
  },

  // otherse redirect to login page
  { path: '**', redirectTo: 'loginComponent' }
];

export const routing = RouterModule.forRoot(appRoutes);
