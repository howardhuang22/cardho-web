import { BaseService } from 'app/service/base.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

// 避免未登入使用者訪問受限制的頁面，在route設定中使用
@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router, private base: BaseService) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.base.user && this.base.token) {
            // logged in so return true
            return true;
        }
 
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/loginComponent'], { queryParams: { returnUrl: state.url }});
        console.log('auth failed, redirect to login page.');
        return false;
    }
}
